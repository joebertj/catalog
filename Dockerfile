FROM php:7.2-alpine3.9

RUN apk update \
    && apk upgrade \
    && docker-php-ext-install mysqli pdo pdo_mysql

WORKDIR /lumen

COPY . /lumen

RUN sed -i "s;APP_URL=http://localhost;APP_URL=https://catalog.kenchlightyear.cloud;" .env \
    && sed -i "s/APP_ENV=local/APP_ENV=production/" .env \
    && sed -i "s/DB_HOST=127.0.0.1/DB_HOST=rds-mysql.default/" .env

CMD ["php", "-S", "localhost:8000", "-t", "public"]

EXPOSE 8000
