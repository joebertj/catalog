# catalog
A lumen product catalog API demo

## Prerequisites

### Install Lumen
composer global require "laravel/lumen-installer"
sudo ln -s ~/.config/composer/vendor/bin/lumen /usr/local/bin/lumen

### Create lumen project
lumen new catalog
cd catalog

### Bootstrap
```
vi .env
./bootstrap.sh
```

## Initial Config

### Enable the DB ORM and Facade
```
sed -i 's;^// $app->withEloquent();$app->withEloquent();' bootstrap/app.php
sed -i 's;^// $app->withFacades();$app->withFacades();' bootstrap/app.php
```

### Increase string length
```
vi app/Provider/AppServiceProvider.php
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
```

## Start PHP server
`php -S 192.168.0.18:8000 -t public`

## API
| HTTP | URL | Description |
|------|-----|-------------|
|GET   |http://192.168.0.18:8000/api/v1/catalog|List all products|
|GET   |http://192.168.0.18:8000/api/v1/catalog/{id}|List a product|
|DELETE|http://192.168.0.18:8000/api/v1/catalog/{id}|Delete a product|

## Build docker image
`docker build -t asia.gcr.io/container-254002/catalog .`

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
