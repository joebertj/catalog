#!/bin/bash

# Load variables
. .env
# Bootstrap the DB
sudo mysql < shop.sql
# Seed the application key
sed -i "s/^APP_KEY=/APP_KEY=`date +%s | sha256sum | base64 | head -c 32 ; echo`/" .env
