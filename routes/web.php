<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1/catalog'], function () use ($router) {
    $router->get('', 'ProductsController@index');
    $router->get('{productId}', 'ProductsController@show');
    $router->post('', 'ProductsController@store');
    $router->delete('{productId}', 'ProductsController@destroy');
});
